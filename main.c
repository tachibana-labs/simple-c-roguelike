#include <SDL2/SDL.h>
#include <stdbool.h>
#include<stdlib.h>

// Game constants
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define TILE_SIZE 32
#define DUNGEON_WIDTH (WINDOW_WIDTH / TILE_SIZE)
#define DUNGEON_HEIGHT (WINDOW_HEIGHT / TILE_SIZE)

// Game state
typedef struct {
    int x;
    int y;
    int level;
} Player;

typedef struct {
    char tiles[DUNGEON_WIDTH][DUNGEON_HEIGHT];
} Dungeon;

// Function prototypes
void handle_events(SDL_Event *event, bool *running, Player *player, Dungeon *dungeon);
void draw_dungeon(SDL_Renderer *renderer, Dungeon *dungeon);
void draw_player(SDL_Renderer *renderer, Player *player);
void move_player(Player *player, Dungeon *dungeon, int dx, int dy);

int main() {
    // Initialize SDL
    SDL_Init(SDL_INIT_VIDEO);

    // Create window and renderer
    SDL_Window *window = SDL_CreateWindow("Roguelike", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    // Initialize game state
    Player player = {DUNGEON_WIDTH / 2, DUNGEON_HEIGHT / 2, 1};
    Dungeon dungeon;
    generate_dungeon(&dungeon);

    // Game loop
    bool running = true;
    SDL_Event event;
    while (running) {
        // Handle events
        while (SDL_PollEvent(&event)) {
            handle_events(&event, &running, &player, &dungeon);
        }

        // Draw dungeon and player
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 128);
        SDL_RenderClear(renderer);
        draw_dungeon(renderer, &dungeon);
        draw_player(renderer, &player);
        SDL_RenderPresent(renderer);

        // Delay to maintain frame rate
        SDL_Delay(1000 / 60);
    }

    // Cleanup
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
// Function definitions
void handle_events(SDL_Event *event, bool *running, Player *player, Dungeon *dungeon) {
    switch (event->type) {
        case SDL_QUIT:
            *running = false;
            break;
        case SDL_KEYDOWN:
            switch (event->key.keysym.sym) {
                case SDLK_UP:
                    move_player(player, dungeon, 0, -1);
                    break;
                case SDLK_DOWN:
                    move_player(player, dungeon, 0, 1);
                    break;
                case SDLK_LEFT:
                    move_player(player, dungeon, -1, 0);
                    break;
                case SDLK_RIGHT:
                    move_player(player, dungeon, 1, 0);
                    break;
            }
            break;
    }
}

void draw_dungeon(SDL_Renderer *renderer, Dungeon *dungeon) {
    for (int x = 0; x < DUNGEON_WIDTH; x++) {
        for (int y = 0; y < DUNGEON_HEIGHT; y++) {
            SDL_Rect rect = {x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE};
            if (dungeon->tiles[x][y] == '#') {
                SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
                SDL_RenderFillRect(renderer, &rect);
            }
            else if (dungeon->tiles[x][y] == '>') {
                SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
                SDL_RenderFillRect(renderer, &rect);
            }
            // Add similar conditions for other tile types as needed
        }
    }
}

void draw_player(SDL_Renderer *renderer, Player *player) {
    SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
    SDL_Rect rect = {player->x * TILE_SIZE, player->y * TILE_SIZE, TILE_SIZE, TILE_SIZE};
    SDL_RenderFillRect(renderer, &rect);
}

void move_player(Player *player, Dungeon *dungeon, int dx, int dy) {
    int new_x = player->x + dx;
    int new_y = player->y + dy;
    if (dungeon->tiles[new_x][new_y] != '#') {
        player->x = new_x;
        player->y = new_y;
        if (dungeon->tiles[new_x][new_y] == '>') {
            player->level++;
            generate_dungeon(dungeon);
        }
        else if (dungeon->tiles[new_x][new_y] == '<' && player->level > 1) {
            player->level--;
        generate_dungeon(dungeon);  // Generate new dungeon for next level
        }
    }

}

#include <stdlib.h> // Include the header file for rand() function

void generate_dungeon(Dungeon *dungeon) {
    // Clear the dungeon
    for (int x = 0; x < DUNGEON_WIDTH; x++) {
        for (int y = 0; y < DUNGEON_HEIGHT; y++) {
            dungeon->tiles[x][y] = '.';
        }
    }

    // Create walls around the edge of the dungeon
    for (int x = 0; x < DUNGEON_WIDTH; x++) {
        dungeon->tiles[x][0] = '#';
        dungeon->tiles[x][DUNGEON_HEIGHT - 1] = '#';
    }
    for (int y = 0; y < DUNGEON_HEIGHT; y++) {
        dungeon->tiles[0][y] = '#';
        dungeon->tiles[DUNGEON_WIDTH - 1][y] = '#';
    }

    // Place the staircase to the next level randomly within the dungeon
    int randomX = rand() % (DUNGEON_WIDTH - 2) + 1; // Random x coordinate within dungeon bounds
    int randomY = rand() % (DUNGEON_HEIGHT - 2) + 1; // Random y coordinate within dungeon bounds
    dungeon->tiles[randomX][randomY] = '>';
}
